#include "output.hpp"

#include <iostream>

#include <unistd.h>

namespace cc
{
  // TODO: This is not guaranteed to be accurate. 
  static bool 
  detect_terminal(std::ostream& os)
  {
    if (&os == &std::cout)
      return isatty(STDOUT_FILENO);
    else if (&os == &std::cerr)
      return isatty(STDERR_FILENO);
    else if (&os == &std::clog)
      return isatty(STDERR_FILENO);    
    else
      return false;
  }

  output_device::output_device(std::ostream& os)
    : os(os), term(detect_terminal(os)), color(term)
  { }

} // namespace cc
