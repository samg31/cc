#pragma once

#include <cc/location.hpp>

#include <cassert>


namespace cc
{
  /// The base class of all nodes in an AST. This class provides facilities
  /// for determining its kind and source range.
  struct node
  {
    node(int k, span locs) 
      : kind(k), locs(locs) { }

    /// The kind of node. This is the discriminator for the terms in the
    /// language. The value -1 is reserved for invalid nodes.
    int kind;

    /// The region of text occupied by the node when it corresponds to a
    /// phrase in a source file. When a term is constructed for the purpose
    /// of translation or analysis, this is typically empty.
    span locs;
  };

// -------------------------------------------------------------------------- //
// Node kind

  /// Returns the kind of the node `n`.
  inline int get_node_kind(const node* n) { return n->kind; }

  /// A traits class facility for testing the type of nodes. This is used by 
  /// the node conversion functions. This must be specialized by derived
  /// node classes in order to work with those facilities.
  template<typename T>
  struct node_info;

  template<>
  struct node_info<node>
  {
    static bool has_kind(const node* n) { return true; }
  };

  /// Returns true if `n` has the kind of node type `T`.
  ///
  /// Example:
  ///
  ///   if (is<add_expr>(p))
  ///     // do stuff with p
  ///
  template<typename T>
  bool 
  is(const node* n)
  {
    return node_info<T>::has_kind(n);
  }

  /// Convert `n` to an object of type `T` if `n` has the kind of node
  /// type `T`. Otherwise, returns nullptr.
  ///
  /// Example:
  ///
  ///   if (const add_expr *e = as<add_expr>(p))
  ///      // do stuff with e
  ///
  template<typename T>
  const T*
  as(const node* n)
  {
    return is<T>(n) ? static_cast<const T*>(n) : nullptr;
  }

  /// Convert `n` to an object of type `T` if `n` has the kind of node
  /// type `T`. Otherwise, returns nullptr.
  ///
  /// Example:
  ///
  ///   if (add_expr *e = as<add_expr>(p))
  ///      // do stuff with e
  ///
  template<typename T>
  T*
  as(node* n)
  {
    return is<T>(n) ? static_cast<T*>(n) : nullptr;
  }

  /// Convert `n` to an object of type `T`. Behavior is undefined if `n`
  /// does not have the kind of node type `T`.
  ///
  /// Example:
  ///
  ///   const add_expr *e = cast<add_expr>(p);
  ///
  /// The program may abort if `p` cannot be converted.
  template<typename T>
  const T*
  cast(const node* n)
  {
    assert(is<T>(n) && "cast to invalid type");
    return static_cast<const T*>(n);
  }

  /// Convert `n` to an object of type `T`. Behavior is undefined if `n`
  /// does not have the kind of node type `T`.
  ///
  /// Example:
  ///
  ///   const add_expr *e = cast<add_expr>(p);
  ///
  /// The program may abort if `p` cannot be converted.
  template<typename T>
  T*
  cast(node* n)
  {
    assert(is<T>(n) && "cast to invalid type");
    return static_cast<T*>(n);
  }

// -------------------------------------------------------------------------- //
// Source locations

  /// Returns the span of text in which a term appears.
  inline span
  get_span(const node* n)
  {
    return n->locs;
  }
  
  /// Returns the (starting) location of a term.
  inline location
  get_location(const node* n)
  {
    return get_span(n).get_start();
  }

} // namespace cc
