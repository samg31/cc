#include "location.hpp"
#include "file.hpp"

#include <iostream>

namespace cc
{
  std::ostream& 
  operator<<(std::ostream& os, coordinates coords)
  {
    return os << coords.line << ':' << coords.column;
  }

  std::ostream& 
  operator<<(std::ostream& os, full_location loc)
  {
    return os << loc.source.get_path() << ':' 
              << loc.coords;
  }

} // namespace cc
