#pragma once

#include <cc/symbol.hpp>

#include <iosfwd>

namespace cc
{
  // The printer is responsible for pretty printing a program.
  class printer
  {
  public:
    printer(std::ostream& os)
      : os(os)
    { }
    
    /// Returns the underlying output stream.
    std::ostream& get_stream() { return os; }

    void print(char c);
    void print(const char* s);
    void print(const std::string& s);
    void print(symbol* s);
    void print(int n);
    void print(const void* p);

    void print_newline() { print('\n'); }
    void print_space() { print(' '); }
    
  protected:
    // An RAII class used to help print language nodes.
    struct sexpr
    {
      sexpr(std::ostream& os, const char* str);
      ~sexpr();

      std::ostream& os;
    };

  protected:
    std::ostream& os;
  };

} // namespace cc